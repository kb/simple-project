from .local import *


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": "test.db",
        "USER": "",
        "PASSWORD": "",
        "HOST": "",
        "PORT": "",
    }
}

# http://docs.celeryproject.org/en/2.5/django/unit-testing.html
BROKER_BACKEND = "memory"
CELERY_ALWAYS_EAGER = True
