source venv-simple-project/bin/activate.fish
set -x DATABASE_HOST ""
set -x DATABASE_PASS ""
set -x DATABASE_USER ""
set -x DJANGO_SETTINGS_MODULE "settings.dev_patrick"
set -x DOMAIN "simple.kbsoftware.co.uk"
set -x HOST_NAME "http://localhost/"
set -x SECRET_KEY "the_secret_key"
source .private
