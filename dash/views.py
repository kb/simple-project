# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
import requests
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.views.generic import TemplateView, CreateView, ListView, UpdateView
from base.view_utils import BaseMixin
from mattermost.models import Channel, ChannelManager
from .forms import (
    ContactForm,
    GameForm,
    GameEmptyForm,
    CompanyForm,
    ContactEmptyForm,
    DogForm,
    DogEmptyForm,
    BreedForm,
)
from django.db.models import Count
from .models import Contact, Game, Company, Dog, Breed, DogManager


class SettingsView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, TemplateView
):

    template_name = "dash/settings.html"


class ContactCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    form_class = ContactForm
    model = Contact

    def get_success_url(self):
        return reverse("contact.list")


class ContactListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    form_class = ContactForm

    def get_queryset(self):
        return Contact.objects.current()


class ContactUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = ContactForm
    model = Contact

    def get_success_url(self):
        return reverse("contact.list")


class ContactDeleteView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BaseMixin,
    UpdateView,
    BaseCommand,
):
    form_class = ContactEmptyForm
    model = Contact

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("contact.list")


class GameCreateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BaseMixin,
    CreateView,
    BaseCommand,
):
    form_class = GameForm
    model = Game

    def form_valid(self, form):
        result = super().form_valid(form)
        text = (
            "Game name, **{}**".format(self.object.name)
            + " Date released, _{}_".format(self.object.date_released)
            + " Genre, **{}**".format(self.object.genre)
            + " Has been created"
        )
        payload = {
            "channel": "activity",
            "username": "dylanmatthews95",
            "icon_url": "https://www.mattermost.org/wp-content/uploads/2016/04/icon.png",
            "text": text,
        }
        r = requests.post(
            "https://chat.pkimber.net/hooks/uoeniqkacbndibahfb5oxgrdbh",
            json=payload,
        )
        # self.stdout.write(str(r.status_code))
        # self.stdout.write("Complete...")
        return result

    def handle(self, *args, **options):
        text = """ 
        """
        payload = {
            "channel": "activity",
            "username": "dylanmatthews95",
            "icon_url": "https://www.mattermost.org/wp-content/uploads/2016/04/icon.png",
            "text": text,
        }
        r = requests.post(
            "https://chat.pkimber.net/hooks/uoeniqkacbndibahfb5oxgrdbh",
            json=payload,
        )
        # self.stdout.write(str(r.status_code))
        # self.stdout.write("Complete...")

    def get_success_url(self):
        return reverse("game.list")


class GameListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    form_class = GameForm

    def get_queryset(self):
        return Game.objects.current()


class GameUpdateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BaseMixin,
    UpdateView,
    BaseCommand,
):
    form_class = GameForm
    model = Game

    def form_valid(self, form):
        result = super().form_valid(form)
        print(self.object.name)
        print(self.object.genre)
        print(self.object.date_released)
        print(self.object.company)
        text = (
            "Game name, **{}**".format(self.object.name)
            + " Date released, _{}_".format(self.object.date_released)
            + " Genre, **{}**".format(self.object.genre)
            + " Has been updated"
        )
        payload = {
            "channel": "activity",
            "username": "dylanmatthews95",
            "icon_url": "https://chat.pkimber.net/api/v4/files/13zcy89r4pfa3y3fyatcbiouyw",
            "text": text,
        }
        r = requests.post(
            "https://chat.pkimber.net/hooks/uoeniqkacbndibahfb5oxgrdbh",
            json=payload,
        )
        # self.stdout.write(str(r.status_code))
        # self.stdout.write("Complete...")
        return result

    def handle(self, *args, **options):
        text = """ 
        Game has been updated
        """
        payload = {
            "channel": "activity",
            "username": "dylanmatthews95",
            "icon_url": "https://www.mattermost.org/wp-content/uploads/2016/04/icon.png",
            "text": text,
        }
        r = requests.post(
            "https://chat.pkimber.net/hooks/uoeniqkacbndibahfb5oxgrdbh",
            json=payload,
        )
        # self.stdout.write(str(r.status_code))
        # self.stdout.write("Complete...")

    def get_success_url(self):
        return reverse("game.list")


class GameDeleteView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BaseMixin,
    UpdateView,
    BaseCommand,
):
    form_class = GameEmptyForm
    model = Game

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("game.list")


class DogCreateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BaseMixin,
    CreateView,
    BaseCommand,
):
    form_class = DogForm
    model = Dog

    def form_valid(self, form):
        result = super().form_valid(form)
        text = (
            "Dog name, **{}**".format(self.object.name)
            + " Date born, _{}_".format(self.object.date_born)
            + " Gender, **{}**".format(self.object.gender)
            + " Has been created"
        )
        channel = Channel.objects.get(name="activity")
        payload = {
            "channel": channel.name,
            "username": self.request.user.username,
            "icon_url": "https://www.mattermost.org/wp-content/uploads/2016/04/icon.png",
            "text": text,
        }
        r = requests.post(channel.url, json=payload)
        # self.stdout.write(str(r.status_code))
        # self.stdout.write("Complete...")
        return result
        # self.stdout.write(str(r.status_code))
        # self.stdout.write("Complete...")
        # return result

    def handle(self, *args, **options):
        text = """ 
        """
        payload = {
            "channel": "activity",
            "username": "dylanmatthews95",
            "icon_url": "https://www.mattermost.org/wp-content/uploads/2016/04/icon.png",
            "text": text,
        }
        r = requests.post(
            "https://chat.pkimber.net/hooks/uoeniqkacbndibahfb5oxgrdbh",
            json=payload,
        )
        # self.stdout.write(str(r.status_code))
        # self.stdout.write("Complete...")

    def get_success_url(self):
        return reverse("dog.list")


class DogListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    form_class = DogForm

    def get_queryset(self):
        return Dog.objects.current()


class DogUpdateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BaseMixin,
    UpdateView,
    BaseCommand,
):
    form_class = DogForm
    model = Dog

    def form_valid(self, form):
        result = super().form_valid(form)
        print(self.object.name)
        print(self.object.gender)
        print(self.object.date_born)
        print(self.object.breed)
        text = (
            "Dog name, **{}**".format(self.object.name)
            + " Date born, _{}_".format(self.object.date_born)
            + " Gender, **{}**".format(self.object.gender)
            + " Has been updated"
        )
        channel = Channel.objects.get(name="activity")
        payload = {
            "channel": channel.name,
            "username": self.request.user.username,
            "icon_url": "https://www.mattermost.org/wp-content/uploads/2016/04/icon.png",
            "text": text,
        }
        r = requests.post(channel.url, json=payload)
        # self.stdout.write(str(r.status_code))
        # self.stdout.write("Complete...")
        return result

    def handle(self, *args, **options):
        text = """ 
        Game has been updated
        """
        payload = {
            "channel": "activity",
            "username": "dylanmatthews95",
            "icon_url": "https://www.mattermost.org/wp-content/uploads/2016/04/icon.png",
            "text": text,
        }
        r = requests.post(
            "https://chat.pkimber.net/hooks/uoeniqkacbndibahfb5oxgrdbh",
            json=payload,
        )
        # self.stdout.write(str(r.status_code))
        # self.stdout.write("Complete...")

    def get_success_url(self):
        return reverse("dog.list")


class DogDeleteView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BaseMixin,
    UpdateView,
    BaseCommand,
):
    form_class = DogEmptyForm
    model = Dog

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("dog.list")


class BreedCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    form_class = BreedForm
    model = Breed

    def get_success_url(self):
        return reverse("breed.list")


class BreedListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    form_class = BreedForm
    model = Breed


class BreedUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = BreedForm
    model = Breed

    def get_success_url(self):
        return reverse("breed.list")


class CompanyCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    form_class = CompanyForm
    model = Company

    def get_success_url(self):
        return reverse("company.list")


class CompanyListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    form_class = CompanyForm
    model = Company


class CompanyUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = CompanyForm
    model = Company

    def get_success_url(self):
        return reverse("contact.list")


class DashView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, TemplateView
):

    template_name = "dash/dash.html"


class BreedView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, TemplateView
):

    template_name = "dash/breed.html"

    def get_context_data(self, **kwargs):
        context = super(BreedView, self).get_context_data(**kwargs)
        # context['breeds'] = Breed.objects.filter(name="Blue-Merle")
        # most_popular_breed =
        dog = Dog.objects.current()
        most_common = Breed.objects.annotate(mc=Count("dog")).order_by("-mc")
        first = most_common.first()
        # print (type(first))
        # import ipdb; ipdb.set_trace()
        # breeds = Breed.objects.filter(name=first)
        # breed = breeds.count()
        context.update(
            {
                "total": Breed.objects.count(),
                "dog": dog.count(),
                "popular": first,
            }
        )
        return context
