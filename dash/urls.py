# -*- encoding: utf-8 -*-
from django.conf.urls import url
from django.urls import path
from . import views
from .views import (
    DashView,
    GameUpdateView,
    SettingsView,
    ContactCreateView,
    ContactListView,
    ContactUpdateView,
    GameCreateView,
    GameListView,
    CompanyCreateView,
    CompanyListView,
    CompanyUpdateView,
    GameDeleteView,
    ContactDeleteView,
    DogCreateView,
    DogDeleteView,
    DogListView,
    DogUpdateView,
    BreedView,
    BreedCreateView,
    BreedListView,
    BreedUpdateView,
)


urlpatterns = [
    url(regex=r"^$", view=DashView.as_view(), name="project.dash"),
    url(regex=r"^breed/$", view=BreedView.as_view(), name="breed.breed"),
    url(
        regex=r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    url(
        regex=r"^contact/create/$",
        view=ContactCreateView.as_view(),
        name="contact.create",
    ),
    url(
        regex=r"^contact/list/$",
        view=ContactListView.as_view(),
        name="contact.list",
    ),
    url(
        regex=r"^contact/(?P<pk>\d+)/update/$",
        view=ContactUpdateView.as_view(),
        name="contact.update",
    ),
    url(
        regex=r"^contact/(?P<pk>\d+)/delete/$",
        view=ContactDeleteView.as_view(),
        name="contact.delete",
    ),
    url(
        regex=r"^game/create/$",
        view=GameCreateView.as_view(),
        name="game.create",
    ),
    url(regex=r"^game/list/$", view=GameListView.as_view(), name="game.list"),
    url(
        regex=r"^game/(?P<pk>\d+)/update/$",
        view=GameUpdateView.as_view(),
        name="game.update",
    ),
    url(
        regex=r"^game/(?P<pk>\d+)/delete/$",
        view=GameDeleteView.as_view(),
        name="game.delete",
    ),
    url(
        regex=r"^dog/create/$", view=DogCreateView.as_view(), name="dog.create"
    ),
    url(regex=r"^dog/list/$", view=DogListView.as_view(), name="dog.list"),
    url(
        regex=r"^dog/(?P<pk>\d+)/update/$",
        view=DogUpdateView.as_view(),
        name="dog.update",
    ),
    url(
        regex=r"^dog/(?P<pk>\d+)/delete/$",
        view=DogDeleteView.as_view(),
        name="dog.delete",
    ),
    url(
        regex=r"^company/create/$",
        view=CompanyCreateView.as_view(),
        name="company.create",
    ),
    url(
        regex=r"^company/list/$",
        view=CompanyListView.as_view(),
        name="company.list",
    ),
    url(
        regex=r"^company/(?P<pk>\d+)/update/$",
        view=CompanyUpdateView.as_view(),
        name="company.update",
    ),
    url(
        regex=r"^breed/create/$",
        view=BreedCreateView.as_view(),
        name="breed.create",
    ),
    url(
        regex=r"^breed/list/$", view=BreedListView.as_view(), name="breed.list"
    ),
    url(
        regex=r"^breed/(?P<pk>\d+)/update/$",
        view=BreedUpdateView.as_view(),
        name="breed.update",
    ),
]
