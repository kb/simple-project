# Generated by Django 2.2 on 2019-05-14 00:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("dash", "0011_auto_20190512_0853")]

    operations = [
        migrations.AlterField(
            model_name="breed",
            name="image",
            field=models.ImageField(
                blank=True,
                max_length=255,
                null=True,
                upload_to="simple-project/%Y/%m/$D/",
            ),
        )
    ]
