# Generated by Django 2.2 on 2019-05-12 02:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("dash", "0007_auto_20190511_2247")]

    operations = [
        migrations.AlterField(
            model_name="breed",
            name="image",
            field=models.ImageField(
                blank=True, null=True, upload_to="images/%Y/%m/$D/"
            ),
        )
    ]
