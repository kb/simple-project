# -*- encoding: utf-8 -*-
from django import forms

from base.form_utils import RequiredFieldForm
from .models import Contact, Game, Company, Dog, Breed


class ContactForm(RequiredFieldForm):
    class Meta:
        model = Contact
        fields = ["name", "phone_number", "note"]


class GameForm(RequiredFieldForm):
    class Meta:
        model = Game
        fields = ["name", "genre", "date_released", "company"]


class GameEmptyForm(RequiredFieldForm):
    class Meta:
        model = Game
        fields = []


class DogForm(RequiredFieldForm):
    class Meta:
        model = Dog
        fields = ["name", "gender", "date_born", "breed"]


class DogEmptyForm(RequiredFieldForm):
    class Meta:
        model = Dog
        fields = []


class BreedForm(RequiredFieldForm):
    class Meta:
        model = Breed
        fields = ["name", "image", "date_discovered"]


class ContactEmptyForm(RequiredFieldForm):
    class Meta:
        model = Contact
        fields = []


class CompanyForm(RequiredFieldForm):
    class Meta:
        model = Company
        fields = ["name", "revenue", "date_established"]
