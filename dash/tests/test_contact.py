# -*- encoding: utf-8 -*-
import pytest

from dash.models import Contact


@pytest.mark.django_db
def test_str():
    contact = Contact(name="Dylan", phone_number="01837 123456")
    contact.save()
    assert "01837 123456" == str(contact)


def test_ordering():
    ordering = Contact(phone_number="phone_number")
    assert "phone_number" == str(ordering)
