import factory
from dash.models import Company, Breed
from datetime import date


class CompanyFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Company

    date_established = date.today()


class BreedFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Breed

    date_discovered = date.today()
