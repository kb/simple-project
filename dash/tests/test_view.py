import pytest

from dash.models import Game, Contact, Company, Dog, Breed
from dash.tests.factories import CompanyFactory, BreedFactory
from datetime import date
from django.urls import reverse

from login.tests.factories import TEST_PASSWORD, UserFactory

from http import HTTPStatus


@pytest.mark.django_db
def test_game_create(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    assert 0 == Game.objects.count()
    company = CompanyFactory(name="Konami")
    url = reverse("game.create")
    response = client.post(
        url,
        {
            "name": "Dylan",
            "genre": "Platformer",
            "date_released": date(2019, 1, 4),
            "company": company.pk,
        },
    )
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert 1 == Game.objects.count()
    game = Game.objects.first()
    assert "Dylan" == game.name
    assert "Platformer" == game.genre
    assert date(2019, 1, 4) == game.date_released


@pytest.mark.django_db
def test_game_list(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    game = Game(
        name="Sonic", genre="platformer", date_released=date(2019, 9, 5)
    )
    game.save()
    url = reverse("game.list")
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_game_update(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    company_1 = CompanyFactory(name="Konami")
    company_2 = CompanyFactory(name="SEGA")
    game = Game(
        name="Sonic",
        genre="platformer",
        date_released=date(2019, 9, 5),
        company=company_1,
    )
    game.save()
    url = reverse("game.update", args=[game.pk])
    response = client.post(
        url,
        {
            "name": "LTTP",
            "genre": "RPG",
            "date_released": date(1991, 3, 6),
            "company": company_2.pk,
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert 1 == Game.objects.count()
    game = Game.objects.first()
    assert "LTTP" == game.name
    assert "RPG" == game.genre
    assert date(1991, 3, 6) == game.date_released
    assert company_2 == game.company


@pytest.mark.django_db
def test_game_delete(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    company_1 = CompanyFactory(name="Konami")
    game = Game(
        name="Sonic",
        genre="platformer",
        date_released=date(2019, 9, 5),
        company=company_1,
    )
    game.save()
    url = reverse("game.delete", args=[game.pk])
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert 1 == Game.objects.count()
    game = Game.objects.first()
    assert game.is_deleted is True


@pytest.mark.django_db
def test_dog_create(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    assert 0 == Dog.objects.count()
    breed = BreedFactory(name="Blue-Merle")
    url = reverse("dog.create")
    response = client.post(
        url,
        {
            "name": "Blue",
            "gender": "male",
            "date_born": date(2012, 1, 4),
            "breed": breed.pk,
        },
    )
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert 1 == Dog.objects.count()
    dog = Dog.objects.first()
    assert "Blue" == dog.name
    assert "male" == dog.gender
    assert date(2012, 1, 4) == dog.date_born


@pytest.mark.django_db
def test_dog_list(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    dog = Dog(name="Blue", gender="male", date_born=date(2011, 9, 5))
    dog.save()
    url = reverse("dog.list")
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_dog_update(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    breed_1 = BreedFactory(name="Blue-Merle")
    breed_2 = BreedFactory(name="Labrador")
    dog = Dog(
        name="Blue", gender="male", date_born=date(2011, 9, 5), breed=breed_1
    )
    dog.save()
    url = reverse("dog.update", args=[dog.pk])
    response = client.post(
        url,
        {
            "name": "Ruby",
            "gender": "female",
            "date_born": date(2005, 3, 6),
            "breed": breed_2.pk,
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert 1 == Dog.objects.count()
    dog = Dog.objects.first()
    assert "Ruby" == dog.name
    assert "female" == dog.gender
    assert date(2005, 3, 6) == dog.date_born
    assert breed_2 == dog.breed


@pytest.mark.django_db
def test_dog_delete(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    breed_1 = BreedFactory(name="Blue-Merle")
    dog = Dog(
        name="Blue", gender="male", date_born=date(2011, 9, 5), breed=breed_1
    )
    dog.save()
    url = reverse("dog.delete", args=[dog.pk])
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert 1 == Dog.objects.count()
    dog = Dog.objects.first()
    assert dog.is_deleted is True


@pytest.mark.django_db
def test_company_create(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    assert 0 == Company.objects.count()
    url = reverse("company.create")
    response = client.post(
        url,
        {
            "name": "Konami",
            "revenue": "$7777777",
            "date_established": date(1970, 1, 4),
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert 1 == Company.objects.count()
    company = Company.objects.first()
    assert "Konami" == company.name
    assert "$7777777" == company.revenue
    assert date(1970, 1, 4) == company.date_established


@pytest.mark.django_db
def test_company_list(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    company = Company(
        name="Konami", revenue="$7777777", date_established=date(1970, 9, 5)
    )
    company.save()
    url = reverse("company.list")
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_company_update(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    company = Company(
        name="Konami", revenue="$7777777", date_established=date(1970, 9, 5)
    )
    company.save()
    url = reverse("company.update", args=[company.pk])
    response = client.post(
        url,
        {
            "name": "Nintendo",
            "revenue": "$999999999",
            "date_established": date(1888, 3, 6),
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert 1 == Company.objects.count()
    company = Company.objects.first()
    assert "Nintendo" == company.name
    assert "$999999999" == company.revenue
    assert date(1888, 3, 6) == company.date_established


# @pytest.mark.django_db
# def test_breed_create(client):
#    user = UserFactory(is_staff=True)
#    assert client.login(username=user.username, password=TEST_PASSWORD) is True
#    assert 0 == Breed.objects.count()
#    url = reverse("breed.create")
#    response = client.post(
#        url,
#        {
#            "name": "Blue-Merle",
# ???         "image": "None",
#            "date_discovered": date(1888, 3, 6),
#        },
#    )
#    assert HTTPStatus.FOUND == response.status_code
#    assert 1 == Breed.objects.count()
#    breed = Breed.objects.first()
#    assert "Blue-Merle" == breed.name
#    assert  "None" == breed.image
#    assert date(1888, 3, 6) == breed.date_discovered


# @pytest.mark.django_db
# def test_breed_list(client):
#    user = UserFactory(is_staff=True)
#    assert client.login(username=user.username, password=TEST_PASSWORD) is True
#    breed = Breed(
# ???     name="Blue-Merle", image=image, date_discovered=date(1888, 3, 6)
#    )
#    breed.save()
#    url = reverse("breed.list")
#    response = client.get(url)
#    assert HTTPStatus.OK == response.status_code


# @pytest.mark.django_db
# def test_breed_update(client):
#    user = UserFactory(is_staff=True)
#    assert client.login(username=user.username, password=TEST_PASSWORD) is True
#    breed = Breed(
# ???     name="Blue-Merle", image=image, date_discovered=date(1888, 3, 6)
#    )
#    breed.save()
#    url = reverse("breed.update", args=[breed.pk])
#    response = client.post(
#        url,
#        {
#            "name": "Labrador",
# ???         "image": image,
#            "date_discovered": date(1970, 3, 6),
#        },
#    )
#    assert HTTPStatus.FOUND == response.status_code
#    assert 1 == Breed.objects.count()
#    breed = Breed.objects.first()
#    assert "Labrador" == breed.name
#    assert image == breed.image
#    assert date(1970, 3, 6) == breed.date_discovered


@pytest.mark.django_db
def test_contact_create(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    assert 0 == Contact.objects.count()
    url = reverse("contact.create")
    response = client.post(
        url, {"name": "Dylan", "phone_number": "04563 192870", "note": "user"}
    )
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert 1 == Contact.objects.count()
    contact = Contact.objects.first()
    assert "Dylan" == contact.name
    assert "04563 192870" == contact.phone_number
    assert "user" == contact.note


@pytest.mark.django_db
def test_contact_list(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = Contact(name="Dylan", phone_number="04563 192870", note="user")
    contact.save()
    url = reverse("contact.list")
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_contact_update(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = Contact(name="Dylan", phone_number="04563 192870", note="user")
    contact.save()
    url = reverse("contact.update", args=[contact.pk])
    response = client.post(
        url, {"name": "Bob", "phone_number": "01928 374650", "note": "admin"}
    )
    assert HTTPStatus.FOUND == response.status_code
    assert 1 == Contact.objects.count()
    contact = Contact.objects.first()
    assert "Bob" == contact.name
    assert "01928 374650" == contact.phone_number
    assert "admin" == contact.note
