# -*- encoding: utf-8 -*-
import pytest

from dash.models import Game, Contact, Dog
from datetime import date
from django.urls import reverse

from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_dash(perm_check):
    url = reverse("project.dash")
    perm_check.staff(url)


@pytest.mark.django_db
def test_settings(perm_check):
    url = reverse("project.settings")
    perm_check.staff(url)


@pytest.mark.django_db
def test_game_list(perm_check):
    url = reverse("game.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_game_create(perm_check):
    url = reverse("game.create")
    perm_check.staff(url)


@pytest.mark.django_db
def test_game_update(perm_check):
    game = Game(
        name="Sonic", genre="platformer", date_released=date(2019, 9, 5)
    )
    game.save()
    url = reverse("game.update", args=[game.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_game_delete(perm_check):
    game = Game(
        name="Sonic", genre="platformer", date_released=date(2019, 9, 5)
    )
    game.save()
    url = reverse("game.delete", args=[game.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_dog_list(perm_check):
    url = reverse("dog.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_dog_create(perm_check):
    url = reverse("dog.create")
    perm_check.staff(url)


@pytest.mark.django_db
def test_dog_update(perm_check):
    dog = Dog(name="Blue", gender="male", date_born=date(2011, 9, 5))
    dog.save()
    url = reverse("dog.update", args=[dog.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_dog_delete(perm_check):
    dog = Dog(name="Blue", gender="male", date_born=date(2011, 9, 5))
    dog.save()
    url = reverse("dog.delete", args=[dog.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_list(perm_check):
    url = reverse("contact.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_create(perm_check):
    url = reverse("contact.create")
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_update(perm_check):
    contact = Contact(name="Sonic", phone_number="04567 023442")
    contact.save()
    url = reverse("contact.update", args=[contact.pk])
    perm_check.staff(url)
