# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
import requests

from mattermostdriver import Driver


class Command(BaseCommand):

    # foo = Driver({
    #'scheme':'https',
    #'port':8065
    # })

    # foo.login()

    # users=['mark','john','steve']

    # text=' '.join(users)

    # foo.webhooks.call_webhook('https://chat.pkimber.net/hooks/uoeniqkacbndibahfb5oxgrdbh', text)

    help = "Post to mattermost"

    def handle(self, *args, **options):
        data = {"text": "Hello, this is some text\nThis is more text. :tada:"}
        text = """ 
#### Test results for July 27th, 2017\n<!channel> please review failed tests.\n
| Component  | Tests Run   | Tests Failed                                   |
|:-----------|:-----------:|:-----------------------------------------------|
| Server     | 948         | :white_check_mark: 0                           |
| Web Client | 123         | :warning: 2 [(see details)](http://linktologs) |
| iOS Client | 78          | :warning: 3 [(see details)](http://linktologs) |
        """
        payload = {
            "channel": "activity",
            "username": "dylanmatthews95",
            "icon_url": "https://www.mattermost.org/wp-content/uploads/2016/04/icon.png",
            "text": text,
        }
        r = requests.post(
            "https://chat.pkimber.net/hooks/uoeniqkacbndibahfb5oxgrdbh",
            json=payload,
        )
        self.stdout.write(str(r.status_code))
        self.stdout.write("Complete...")
