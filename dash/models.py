# -*- encoding: utf-8 -*-
from __future__ import with_statement
import datetime

# import webapp2

from django.db import models
from django.db.models.query import QuerySet
from django.utils import timezone
from base.model_utils import TimedCreateModifyDeleteModel
from django.core.files.storage import Storage

# from imagekit.models import ImageSpecField
# from imagekit.processors import ResizeToFill
# from base64 import b64decode
# from io import StringIO
# from xmlrpc.server import XMLRPCDocGenerator

# from google.appengine.api import files
# from google.appengine.api import images
# from google.appengine.ext import blobstore
# from google.appengine.ext.webapp import blobstore_handlers
# form_class = GameForm

# class ImageRpcServer(object):

#   def upload(self, meta, photo, mime_type):
#      data = b64decode(photo)
#     file_name = files.blobstore.create(mime_type=mime_type)
#    with files.open(file_name, 'a') as f:
#       f.write(data)
#  files.finalize(file_name)
# key = files.blobstore.get_blob_key(file_name)
# return str(key)

# def serving_url(self, meta, key):
#   return images.get_serving_url(blobstore.BlobKey(key))


class SoftDeletionQuerySet(QuerySet):
    def delete(self):
        # Bulk delete bypasses individual objects' delete methods.
        return super(SoftDeletionQuerySet, self).update(alive=False)

    def hard_delete(self):
        return super(SoftDeletionQuerySet, self).delete()

    def alive(self):
        return self.filter(alive=True)

    def dead(self):
        return self.exclude(alive=True)


class SoftDeletionManager(models.Manager):
    def __init__(self, *args, **kwargs):
        self.alive_only = kwargs.pop("alive_only", True)
        super(SoftDeletionManager, self).__init__(*args, **kwargs)

    def get_queryset(self):
        if self.alive_only:
            return SoftDeletionQuerySet(self.model).filter(alive=True)
        return SoftDeletionQuerySet(self.model)

    def hard_delete(self):
        return self.get_queryset().hard_delete()


class SoftDeletionModel(models.Model):
    alive = models.BooleanField(default=True)

    objects = SoftDeletionManager()
    all_objects = SoftDeletionManager(alive_only=False)

    class Meta:
        abstract = True

    def delete(self):
        self.alive = False
        self.save()

    def hard_delete(self):
        super(SoftDeletionModel, self).delete()


class ContactManager(models.Manager):
    def current(self):
        return self.model.objects.all().exclude(deleted=True)


class Contact(TimedCreateModifyDeleteModel):
    company = models.ForeignKey("Company", on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=25)
    note = models.CharField(max_length=200)

    objects = ContactManager()

    class Meta:
        ordering = ("name", "phone_number")

    def __str__(self):
        return self.phone_number


class GameManager(models.Manager):
    def current(self):
        return self.model.objects.all().exclude(deleted=True)


class Game(TimedCreateModifyDeleteModel):
    company = models.ForeignKey("Company", on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=100)
    genre = models.CharField(max_length=25)
    date_released = models.DateField()
    objects = GameManager()

    class Meta:
        ordering = ("name", "genre", "date_released")

    def __str__(self):
        return self.name


class DogManager(models.Manager):
    def current(self):
        return self.model.objects.all().exclude(deleted=True)


class Dog(TimedCreateModifyDeleteModel):
    breed = models.ForeignKey("Breed", on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=100)
    gender = models.CharField(max_length=25)
    date_born = models.DateField()
    objects = DogManager()

    class Meta:
        ordering = ("breed", "name", "gender", "date_born")

    def __str__(self):
        return self.name


class AppEngineBlobStorage(Storage):
    def exists(self, name):
        ...

    def size(self, name):
        ...

    def url(self, name):
        ...

    def delete(self, name):
        ...

    def listdir(self, path):
        raise NotImplementedError()


class Breed(models.Model):
    name = models.CharField(max_length=100)
    image = models.ImageField(
        blank=True, null=True, upload_to="media/%Y/%m/$D/", max_length=255
    )
    date_discovered = models.DateField()
    # sample_breed = models.FileField(blank=True, null=True, upload_to="breeds/%Y/%m/$D/")

    class Meta:
        ordering = ("name", "image", "date_discovered")

    def __str__(self):
        return self.name

    pass


class Company(models.Model):
    name = models.CharField(max_length=100)
    revenue = models.CharField(max_length=25)
    date_established = models.DateField()

    class Meta:
        ordering = ("name", "revenue", "date_established")

    def __str__(self):
        return self.name

    pass


# game = Game.objects.create_game("Super Smash Bros. Melee", "Fighting game", "2002")
