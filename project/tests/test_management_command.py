# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command


@pytest.mark.django_db
def test_demo_data_project():
    call_command("init_project")
    call_command("demo_data_project")


@pytest.mark.django_db
def test_init_project():
    call_command("init_project")
