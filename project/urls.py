# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.static import serve
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static

admin.autodiscover()


urlpatterns = [
    path("admin/", admin.site.urls),
    url(regex=r"^", view=include("login.urls")),
    url(regex=r"^", view=include("web.urls")),
    url(regex=r"^dash/", view=include("dash.urls")),
    url(regex=r"^mail/", view=include("mail.urls")),
    url(regex=r"^mattermost/", view=include("mattermost.urls"))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#   ^ helper function to return a URL pattern for serving files in debug mode.
# https://docs.djangoproject.co0m/en/1.5/howto/static-files/#serving-files-uploaded-by-a-user

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        url(r'^media/(?P<path>.*)$', serve, {
            'document.root': settings.MEDIA_ROOT,
        }),
            url(r'^__debug__/', include(debug_toolbar.urls)),
    ] 
