-r base.txt
-e .
-e ../../app/base
-e ../../app/login
-e ../../app/mail
-e ../../app/mattermost
black
django-debug-toolbar
factory-boy
pytest-cov
pytest-django
pytest-flakes
